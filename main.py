import json
import requests
import argparse

parser = argparse.ArgumentParser()

parser.add_argument("-fuzz", "--fuzziness")
parser.add_argument("-m", "--fuzzy_min_length")
parser.add_argument("-size", "--size")
parser.add_argument("-l", "--fuzzy_prefix_length")
parser.add_argument("-pref", "--prefix")

args = parser.parse_args()
prefix = args.prefix if args.prefix else 'ange'
size = args.size if args.size else 5
fuzzy_min_length = args.fuzzy_min_length if args.fuzzy_min_length else 3
fuzzy_prefix_length = args.fuzzy_prefix_length if args.fuzzy_prefix_length else 1

fuzziness = args.fuzziness if args.fuzziness else 'auto:1,3'

index = 'dictionary'
headers = {'content-type': 'application/json'}

data = json.dumps(
    {
        "suggest": {
            "word_suggest": {
                "prefix": prefix,
                "completion": {
                    "field": "word",
                    "size": size,
                    "skip_duplicates": "true",
                    "fuzzy": {
                        "fuzziness": fuzziness,
                        "transpositions": "true",
                        "min_length": fuzzy_min_length,
                        "prefix_length": fuzzy_prefix_length
                    }
                }
            }
        }
    }
)

req = requests.get(f'http://localhost:3000/{index}/search', headers=headers, data=data)
if req.status_code == 200:
    options = req.json()['suggest']['word_suggest'][0]['options']
    if len(options) > 0:
        for i in range(len(options)):
            print(options[i]['text'])
    else:
        print('No match')
else:
    raise Exception("Error")